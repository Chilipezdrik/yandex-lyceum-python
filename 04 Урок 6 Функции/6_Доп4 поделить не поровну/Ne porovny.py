n = int(input())
digit_max = max(n % 10, n // 10 % 10, n // 100 % 10, n // 1000 % 10)
digit_min = min(n % 10, n // 10 % 10, n // 100 % 10, n // 1000 % 10)
n2_s = ""
for i in range(4):
    cur_digit = n // 10 ** i % 10
    if cur_digit != digit_max and cur_digit != digit_min:
        n2_s = n2_s + str(cur_digit)        
n2 = int(n2_s)
if n2 % 10 > n2 // 10:
    digit_max_2 = n2 % 10
    digit_min_2 = n2 // 10
else:
    digit_max_2 = n2 // 10
    digit_min_2 = n2 % 10
if digit_min == 0:
    digit_min, digit_min_2 = digit_min_2, digit_min
print(str(digit_min) + str(digit_min_2), str(digit_max) + str(digit_max_2))
