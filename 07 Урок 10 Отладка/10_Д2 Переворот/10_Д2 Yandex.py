n = int(input())
res = ''
while n:
    if not (n % 10 == 0 and res == ''):
        res += str(n % 10)
    n //= 10
print(res)