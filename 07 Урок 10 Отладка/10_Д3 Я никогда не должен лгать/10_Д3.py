s = last_entered = input()
while s != '':
    if len(s) > 99 and len(s) < 1000:
        print(last_entered)
    elif len(s) % 2 == 0:
        print(s * 2)
    elif len(s) % 3 == 0:
        print(s * 3)
    else:
        print(s)
    last_entered = s
    s = input()