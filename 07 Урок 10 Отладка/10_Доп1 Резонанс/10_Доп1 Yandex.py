number_1, number_2 = input(), input()
size = len(number_1)
number_1, number_2 = int(number_1), int(number_2)
result = 0
if size == 4:
    if number_2 == number_1 + 1000 or number_1 == number_2:
        result = 1
    elif number_2 == number_1 - 1:
        result = 2
    elif number_2 == number_1 % 10 * 1000 + number_1 // 10:
        result = 3
    elif number_2 == number_1 % 1000 * 10 + number_1 // 1000:
        result = 4
elif size == 5:
    if number_2 == number_1 + 10000:
        result = 1
    elif number_2 == number_1 - 1:
        result = 2
    elif number_2 == number_1 % 10 * 10000 + number_1 // 10:
        result = 3
    elif number_2 == number_1 % 10000 * 10 + number_1 // 10000:
        result = 4
if result:
    print("ДА", result)
else:
    print("НЕТ")