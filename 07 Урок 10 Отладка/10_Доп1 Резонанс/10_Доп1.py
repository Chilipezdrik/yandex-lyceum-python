nat_freq = nat_freq_saved = int(input())
force_freq = int(input())
digit_count = n0 = n1 = n2 = n3 = n4 = 0
while nat_freq > 0:
    digit_count += 1
    nat_freq //= 10
nat_freq = nat_freq_saved
if digit_count == 4:
    n0 = nat_freq // 1000
    n1 = nat_freq // 100 % 10
    n2 = nat_freq // 10 % 10
    n3 = nat_freq % 10
if digit_count == 5:
    n0 = nat_freq // 10000 % 10
    n1 = nat_freq // 1000 % 10
    n2 = nat_freq // 100 % 10
    n3 = nat_freq // 10 % 10
    n4 = nat_freq % 10
if digit_count == 4:
    if force_freq == n1 * 1000 + n2 * 100 + n3 * 10 + n0:
        print("ДА 4")
    elif force_freq == n3 * 1000 + n0 * 100 + n1 * 10 + n2:
        print("ДА 3")
    elif force_freq == n3 * 1000 + n2 * 100 + n1 * 10 + n0 - 1:
        print("ДА 2")
    elif force_freq == (n3 + 1)  * 1000 + n2 * 100 + n1 * 10 + n0 and n3 != 9:
        print("ДА 1")
    else:
        print("НЕТ")
elif force_freq == n1 * 10000 + n2 * 1000 + n3 * 100 + n4 * 10 + n0:
    print("ДА 4")
elif force_freq == n4 * 10000 + n0 * 1000 + n1 * 100 + n2 * 10 + n3:
    print("ДА 3")
elif force_freq == n4 * 10000 + n3 * 1000 + n2 * 100 + n1 * 10 + n0 - 1:
    print("ДА 2")
elif force_freq == (n4 + 1) * 10000 + n3 * 1000 + n2 * 100 + n1 * 10 + n0 and n4 != 9:
    print("ДА 1")
else:
    print("НЕТ")