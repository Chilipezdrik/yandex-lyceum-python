side = float(input())
speed = float(input())
turn_count = 0
while side - speed > 0.01:
    turn_count += 1
    side = ((side - speed) ** 2 + speed ** 2) ** 0.5
print(turn_count)
