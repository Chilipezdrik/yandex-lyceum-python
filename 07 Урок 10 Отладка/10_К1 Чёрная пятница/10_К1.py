n = int(input())
if n == 0:
    print(50)
elif n > 0 and n <= 30:
    print(int(n + n * 0.5))
elif n > 30 and n <= 70:
    print(int(n + n * 0.1))
else:
    print(n)