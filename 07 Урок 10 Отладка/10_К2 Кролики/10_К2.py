n = int(input())
fib_cur = 0
fib_next = 1
generation = 1

while fib_next != n and fib_next < n:
    generation += 1
    fib_cur, fib_next = fib_next, fib_cur + fib_next
if n == fib_next:
    print(generation)
else:
    print("НЕТ")