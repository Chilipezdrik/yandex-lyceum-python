number = int(input())
if number == 0:
    print(1)
else:
    for i in range(number):
        a = number + i
        flag_zeros = False
        while a > 0:
            if a % 2 == 0:
                flag_zeros = True
                break
            else:
                a = a // 2
        if not flag_zeros:
            print(i)