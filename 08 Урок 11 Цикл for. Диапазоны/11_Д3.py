for i in range(int(input()), int(input()) + 1):
    n0, n1, n2, n3 = i // 1000 % 10, i // 100 % 10, i // 10 % 10, i % 10
    if n0 != n1 and n0 != n2 and n0 != n3 and n1 != n2 and n1 != n3 and n2 != n3:
        print(i)