s_len_sum, result, result_1, result_2 = 0, 0, 0, 0
for i in range(2):
    word = input()
    s_num = int(input())
    s_len_sum = 0
    for j in range(s_num):
        s_cur = input()
        if word in s_cur:
            s_len_sum += len(s_cur)
    if i == 0:
        result_1 = s_len_sum / len(word)
    else:
        result_2 = s_len_sum / len(word)
if result_1 > result_2:
    print('Больше в первом случае на', result_1 - result_2)
else:
    print('Больше во втором случае на', result_2 - result_1)