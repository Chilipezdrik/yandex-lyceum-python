s1, s2, s3, step_direction = input(), input(), input(), 1
if len(s1) + len(s2) > len(s2) + len(s3):
    step_direction = -1
for i in range(len(s1) + len(s2), len(s2) + len(s3) + step_direction, step_direction * min(len(s1), len(s2), len(s3))):
    print(i, end=' ')
