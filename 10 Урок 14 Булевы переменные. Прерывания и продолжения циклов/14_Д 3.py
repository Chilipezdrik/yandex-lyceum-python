s, count, star_present, star_count = input(), 0, False, 0
while s != 'ВСЁ':
    count += 1
    if 'звезд' in s or 'Звезд' in s:
        star_present = True
        star_count = count
    if 'пал' in s or 'пад' in s:
        star_present = False
    s = input()
if star_present:
    print(star_count)
else:
    print('НЕТ')