num = int(input())
obj = input()
elephant = False
legs = eyes = ears = hobot = tail = mouth = 0
while obj != 'ОБЕД':
    if obj == 'хобот':
        hobot += num
    elif obj == 'хвост':
        tail += num
    elif obj == 'нога':
        legs += num
    elif obj == 'ухо':
        ears += num
    elif obj == 'глаз':
        eyes += num
    elif obj == 'рот':
        mouth += num
    if hobot and tail and legs > 3 and ears >= 2 and eyes >= 2 and mouth:
        el_count = min(hobot, tail, legs // 4, ears // 2, eyes // 2, mouth)
        elephant = True
        print('Есть слон!')
        print(el_count)
        break
    num = int(input())
    obj = input()
if not elephant:
    print('Какие-то слоны нецелые. Пошли обедать.')