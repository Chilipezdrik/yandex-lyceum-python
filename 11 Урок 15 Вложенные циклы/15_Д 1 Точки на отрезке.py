x1, y1, x2, y2 = int(input()), int(input()), int(input()), int(input())
tan = (max(y1, y2) - min(y1, y2)) / (max(x1, x2) - min(x1, x2))
count = 1
for i in range(min(x1, x2) + 1, max(x1, x2) + 1):
    for j in range(min(y1, y2), max(y1, y2) + 1):
        if (j - min(y1, y2)) / (i - min(x1, x2)) == tan:
            count += 1
print(count)