cars = int(input())
cur_car_is_overspeed = False
previous_car_is_overspeed = False
at_least_one_car_is_overspeed = False
all_cars_is_overspeed = True
max_speed = 0
previous_max_speed = 0
for car in range(cars):
    car_measures = int(input())
    for measure in range(car_measures):
        car_speed = int(input())
        if car_speed > 60:
            at_least_one_car_is_overspeed = True
            cur_car_is_overspeed = True
        elif car_speed > max_speed and not cur_car_is_overspeed:
            max_speed = car_speed
    if cur_car_is_overspeed:
        max_speed = previous_max_speed
    else:
        previous_max_speed = max_speed
    if not cur_car_is_overspeed and not previous_car_is_overspeed:
        all_cars_is_overspeed = False
    cur_car_is_overspeed = False
if all_cars_is_overspeed:
    print(0, 'ДА', sep='\n')
else:
    print(max_speed)
    if at_least_one_car_is_overspeed:
        print('ДА')
    else:
        print('НЕТ')
