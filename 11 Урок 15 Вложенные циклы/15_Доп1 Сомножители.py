number = number_changed = int(input())
answer_string = str(number) + ' = '
for delitel in range(2, number):
    while number % delitel == 0:
        if number_changed == number:
            answer_string += str(delitel)
            number = number / delitel
        else:
            answer_string = answer_string + ' * ' + str(delitel)
            number = number / delitel
print(answer_string)
