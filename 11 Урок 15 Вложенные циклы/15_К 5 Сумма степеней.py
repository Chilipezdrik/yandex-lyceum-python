number, summa, even_power, odd_power = int(input()), 0, 0, 0
for n in range(1, number + 1):
    if n % 2 == 0:
        even_power += n
        summa += n ** even_power
    else:
        odd_power += n
        summa += n ** odd_power
print(summa)