direction = input()
date = int(input())
if 1 <= date <= 31:
    if direction == 'с запада на восток':
        print(date, 'OK')
    elif direction == 'с востока на запад':
        if (date + 2) % 31 == 0:
            print(31)
        else:
            print((date + 2) % 31)
else:
    print("Ошибочка у вас.")