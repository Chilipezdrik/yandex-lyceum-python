shodna_capacity = int(input())
palubs = int(input())
paluba_people_min = 99999999999
paluba_people_max = 0
for i in range(1, palubs + 1):
    paluba_people_sum = 0
    cabins_for_paluba = int(input())
    for j in range(1, cabins_for_paluba + 1):
        cabin_people = int(input())
        paluba_people_sum += cabin_people
    if paluba_people_sum > paluba_people_max:
        paluba_people_max = paluba_people_sum
    if paluba_people_sum < paluba_people_min:
        paluba_people_min = paluba_people_sum
    if paluba_people_sum % shodna_capacity == 0:
        print('=' * (paluba_people_sum // shodna_capacity))
    else:
        print('=' * ((paluba_people_sum // shodna_capacity) + 1))
print(paluba_people_max, paluba_people_min)