#!/usr/local/bin/python3.7
import sys

user_set, answer_set = set(), set()

# for i in open(sys.argv[2]):
#     user_set.add(i.strip())
# for i in open(sys.argv[3]):
#     answer_set.add(i.strip())
user_set = set(open(sys.argv[2]).readline().strip())
answer_set = set(open(sys.argv[3]).readline())

if (user_set == answer_set):
    print('1 OK')
    sys.exit(0)
else:
    print('0 WA')
    print('Одидалось', answer_set)
    print('Вывелось', user_set)
    sys.exit(1)
