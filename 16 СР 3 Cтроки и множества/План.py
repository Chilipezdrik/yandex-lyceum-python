potion = input()
antidote = input()
containers = int(input())
answer_set = set()
for i in range(containers):
    m = int(input())
    for j in range(m):
        ingredient = input()
        # чётные котлы - для зелья potion
        # нечётные котлы - для противоядия antidote
        if i % 2 == 0:
            if len(potion) % len(ingredient) == 0:
                if set(potion) & set(ingredient):
                    answer_set.add(ingredient[:5])
        else:
            if len(antidote) % len(ingredient) != 0:
                if antidote in ingredient:
                    answer_set.add(ingredient[:5])