string_max_len = 0
strings = []
string = input()
while string:
    strings.append(string)
    if len(string) > string_max_len:
        string_max_len = len(string)
    string = input()

for i in range(len(strings)):
    if len(strings[i]) < string_max_len:
        if (string_max_len - len(strings[i])) % 2 == 0:  # добавить чётное число дефисов
            strings[i] = '-' * ((string_max_len - len(strings[i])) // 2) + strings[i]\
                         + '-' * ((string_max_len - len(strings[i])) // 2)
        else:  # добавить нечётное число дефисов
            strings[i] = '-' * ((string_max_len - len(strings[i])) // 2) + strings[i]\
                         + '-' * (((string_max_len - len(strings[i])) // 2) + 1)
print(*strings[::-1], sep='\n')
