line = input().split(', ')
gifts = ["flower", "gemstone"]
print('; '.join([gifts[i % 2] for i in range(len(line))]))