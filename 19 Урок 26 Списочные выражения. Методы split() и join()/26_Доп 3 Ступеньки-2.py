s = ' '.join([str(i) + '; АХ!' if i % 4 == 0 else str(i) + ',' for i in range(1, int(input()) + 1)])
print(s if s[-1] == '!' else s[:-1])
