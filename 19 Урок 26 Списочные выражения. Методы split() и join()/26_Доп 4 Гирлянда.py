strings = input().split()
max_word_len = 0
max_word = string_of_index = ''
answer = []
#  Находим максимальную длину слова и само слово в строке
for i in range(len(strings)):
    if len(strings[i]) > max_word_len:
        max_word_len = len(strings[i])
        max_word = strings[i]

'''
Пробегаемся по индексам в каждом слове исходной строки и записываем их в строку.
Далее добавляем эту строку, как элемент списка 'answer'
'''
for s_index in range(max_word_len):
    for string in strings:
        if s_index >= len(string):
            string_of_index += ' '
            continue
        else:
            string_of_index += string[s_index]
    answer.append(string_of_index)
    string_of_index = ''

# Преобразование списка 'answer' в нужный формат
for i in range(len(answer)):
    if i == 0:
        answer[i] = '_' + '_'.join(answer[i]) + '_'
    else:
        answer[i] = ' ' + ' '.join(answer[i])

# Вывод ответа
print(*answer, sep='\n')
print()
print(*answer[::-1], sep='\n')
