times = [int(i) for i in input().split()]
intervals = [int(i) for i in input().split()]
intervals.sort()
print(sum(times[intervals[0]:intervals[1]]))