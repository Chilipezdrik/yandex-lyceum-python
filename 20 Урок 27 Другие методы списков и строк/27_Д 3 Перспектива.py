strings = input().split()
answer = list()
for i in range(len(strings)):
    strings[i] = int(strings[i])
cur_max = strings[0]
answer.append(strings[0])
for i in range(1, len(strings)):
    if strings[i] > cur_max:
        cur_max = strings[i]
        answer.append(strings[i])
print(*answer, sep='>>')
