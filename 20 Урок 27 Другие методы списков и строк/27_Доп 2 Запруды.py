s_word = input()
set_letters = set()
index = 0
parts = list()

while index < len(s_word):
    if s_word[index] not in set_letters:
        print(f'текущая буква - {s_word[index]}', end=' ')
        set_letters.add(s_word[index])
        if s_word.count(s_word[index]) > 1:
            print('встречалась больше 1 раза')
            first_let_index = s_word.find(s_word[index])
            last_let_index = s_word.rfind(s_word[index])
            parts.append(s_word[first_let_index:last_let_index + 1])
            set_letters.update(s_word[first_let_index:last_let_index + 1])
            index = s_word.rfind(s_word[index]) + 1
        else:
            print('встречалась только 1 раз')
            parts.append(s_word[index])
            index += 1
    else:
        print(f'текущая буква - {s_word[index]} уже была в прошлой части')
        for i in range(len(parts)):
            if s_word[index] in parts[i]:
                first_letter = parts[i][0]
                last_letter = s_word[index]
                print(f'first_letter = {first_letter}')
                print(f'last_letter = {last_letter}')
                parts = parts[:i]
                parts.append(s_word[s_word.find(first_letter):s_word.rfind(s_word[index]) + 1])
                break
        index = s_word.rfind(s_word[index]) + 1
        print('parts has been repaired')
        print(index)
    print(f'parts = {parts}')
    print(f'letters = {set_letters}')

parts_is_intersected = False
for i in range(len(parts) - 1):
    if bool(set(parts[i]) & set(parts[i + 1])):
        parts_is_intersected = True
        break

if parts_is_intersected:
    print(len(s_word))
else:
    for i in range(len(parts)):
        print(len(parts[i]), end=' ')

# tests
# assembler - 1 2 5 1
# remember - 8
# eyeeywyreqr - 11
# mmmhhhkkkdddfffdddmmmhhhfffggg - 27 3
# teyeeqeyerwwy - 1 12
# bbbnnneeebbbgggnnnoooaaammmggg - 30