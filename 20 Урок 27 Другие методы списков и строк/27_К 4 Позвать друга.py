string = input()
strings = list()
answer = list()
while string:
    strings.append(string)
    string = input()
name = input()
for i in range(len(strings)):
    if strings[i].startswith('@' + name):
        answer.append(strings[i][len(name) + 2:].capitalize())
answer.sort()
print(*answer, sep='\n')
