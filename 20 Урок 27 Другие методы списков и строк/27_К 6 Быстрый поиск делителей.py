number = int(input())
answer = list()
for i in range(1, int(number ** 0.5) + 1):
    if number % i == 0:
        answer.append(i)
        answer.append(number // i)
answer.sort()
print(*answer)
