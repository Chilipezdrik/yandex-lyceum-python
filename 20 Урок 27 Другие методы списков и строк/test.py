#
# d = {'31' : [12, 23, 45], '64' :[67, 87, 98]}
# print(d['31'])
#
# parts = ['a', 'б', 'в']
# print('к' in parts)

# letters = set()
# s_word = 'Hello world'
# # letters.update(''.join(s_word.split()))
# # letters.update(s_word)
# print(bool(letters))


string = str(input()).split()

max_len = 0

for i in range(len(string)):
    if len(string[i]) > max_len:
        max_len = len(string[i])

for i in range(max_len):
    for j in range(len(string)):
        if i == 0:
            print('_' + string[j][i], end='')
        elif i >= len(string[j]):
            print('  ', end='')
        else:
            print(' ' + string[j][i], end='')
    if i == 0:
        print('_')
    else:
        print()

for i in range(max_len, -1, -1):
    for j in range(len(string)):
        if i == 0:
            print('_' + string[j][i], end='')
        elif i >= len(string[j]):
            print('  ', end='')
        else:
            print(' ' + string[j][i], end='')
    if i == 0:
        print('_')
    else:
        print()
