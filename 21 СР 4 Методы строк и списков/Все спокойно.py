melodies = input().split('&')
for i in range(len(melodies)):
    if ' ' in melodies[i] and ''.join(melodies[i].split()).isalnum():
        melodie_words = melodies[i].split()
        first_word_list = list(melodie_words[0].lower())
        last_word_list = list(melodie_words[-1].lower())

        for ii in range(len(first_word_list)):
            if first_word_list[ii].isalpha():
                first_word_list[ii] = first_word_list[ii].upper()
                break
        for ii in range(len(last_word_list)):
            if last_word_list[ii].isalpha():
                last_word_list[ii] = last_word_list[ii].upper()
                break
        melodies[i] = ''.join(first_word_list) + ' ' + ''.join(last_word_list)

    elif len(melodies[i]) % 3 == 0 and len(melodies[i].split()) == 1:
        melodie = list(melodies[i])
        for j in range(0, len(melodie), 3):
            if melodie[j].islower():
                melodie[j] = melodie[j].upper()
            else:
                melodie[j] = melodie[j].lower()
        melodies[i] = ''.join(melodie[0:len(melodie):3])
    else:
        melodies[i] = melodies[i][::-1].upper()
print(', '.join(melodies))
