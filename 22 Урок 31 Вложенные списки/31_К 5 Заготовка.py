war_map = [[0] * 10 for _ in range(10)]
ship = '\u25A1'
# for i in range(10):
#     for j in range(10):
#         print(war_map[i][j], end=' ')
#     print()

# debug
# cords = input().split()
# print(int(cords[0][1:]) - 1)
# war_map[int(cords[0][1:]) - 1][ord(cords[0][0]) - 1072] = ship
# war_map[4][0] = ship

for i in range(1, 11):
    cords = input().split()
    #  print(f'выставляю {i} - ый корабль\n координаты - {cords}')
    if 'к' in cords[0]:
        cords[0] = 'й' + cords[0][-1]
    if i <= 1:  # 4-ех палубные корабли
        if cords[1] == 'в':
            for j in range(4):
                war_map[int(cords[0][1:]) - 1 + j][ord(cords[0][0]) - 1072] = ship
        else:
            for j in range(4):
                war_map[int(cords[0][1:]) - 1][ord(cords[0][0]) - 1072 + j] = ship
    if 2 <= i < 4:  # 3-ех палубные корабли
        if cords[1] == 'в':
            for j in range(3):
                war_map[int(cords[0][1:]) - 1 + j][ord(cords[0][0]) - 1072] = ship
        else:
            for j in range(3):
                war_map[int(cords[0][1:]) - 1][ord(str(cords[0][0])) - 1072 + j] = ship
    if 4 <= i < 7:  # 2-ух палубные корабли
        if cords[1] == 'в':
            for j in range(2):
                war_map[int(cords[0][1:]) - 1 + j][ord(cords[0][0]) - 1072] = ship
        else:
            for j in range(2):
                war_map[int(cords[0][1:]) - 1][ord(cords[0][0]) - 1072 + j] = ship
    else:  # 1 - однопалубные корабли
        war_map[int(cords[0][1:]) - 1][ord(cords[0][0]) - 1072] = ship
    # print('После выставления: ')
    # for i in range(10):
    #     for j in range(10):
    #         print(war_map[i][j], end=' ')
    #     print()
# print()
for i in range(10):
    for j in range(10):
        print(war_map[i][j], end=' ')
    print()
