timetable = dict()
s = input()
while s:
    cur_bus = s.split()[0]
    bus_time = int(s.split()[1].split(':')[0]) * 60 + int(s.split()[1].split(':')[1])
    if cur_bus not in timetable:
        timetable[cur_bus] = [bus_time]
    else:
        timetable[cur_bus] += [bus_time]
    s = input()

cur_time = input().split(':')
cur_time = int(cur_time[0]) * 60 + int(cur_time[1])
wanted_busses = input().split()

exit_time = 0
for bus in wanted_busses:
    for time in timetable[bus]:
        if time - 6 >= cur_time:
            exit_time = time - 6 - cur_time
            break
if exit_time:
    print(exit_time)
else:
    print('None')

# print(min(5, 4))
# my_tuple = (18, )
# print(my_tuple)
# my_tuple = my_tuple + (3, )
# print(f'my_tuple * 2 = {my_tuple * 2}')
# print(f'my_tuple * 2 + tuple(5) = {my_tuple * 2 + (5, )}')
# d = dict()
# d['Meg'] = 24
# d['Nea'] = 25
# d['Thomas'] = 25
# print(d)
# print(len(d))
# card = ('7','пик')
# t = (18,)
# print(card)
# print(card + t)

#
# d = {'31' : [12, 23, 45], '64' :[67, 87, 98]}
# print(d['31'])
#
#
# a = '0xA5eAEf66eaDCD0DbC0145ec6459387b39F3063db'
# b = '0xA5eAEf66eaDCD0DbC0145ec6459387b39F3063db'
# print(a == b)

# my_buy_list = [
#     40 * 3, # икра
#     42, # соус тонких трав
#     19,# финики
#     167,# мандарины
#     65,# икра (стеклянная)
#     40 * 3,# хлеб вавилон ржаной
#     106,# помидоры
#     64,# огурцы среднеплодные
#     67,# хлеб мультизлак нарезной
#     96,# бурёк
#     33,# слойка с малиной и грушей
#     220,# слойка с сыром (сырная палочка)
#     30,# пампушки с чесноком
#     25,# пряные с чесноком
#     40 * 2,# улитки
#     229,# королевские грибы
#     89,# оливки маленькие
#     109,# оливки большие
#     111,# хлебные палочки
#     35 * 3,# перловка
#     40,# приправа чесночный перец
#     40,# приправа куркума
#     55,# хлебцы молодцы томатные
#     43,# фруттилад
#     95,# марный кувшин
#     11,# мыло ландыш
#     82,# кутчуп
#     27,# горчица
#     90 * 3,# лампы светодиодные теплые
#     90,# лампа светодиодныая хол
#     68,# термометр
#     90,# лампа светодиодная еак
#     50 * 3,# мыло туалетное семейное
#     62,# чеснок гранулированный
#     80,# печенье овсяное
#     60# пемолякс
# ]
# print(sum(my_buy_list))
# print(len(my_buy_list))
